﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgrammingLanguageEnvironment
{
    /// <summary>
    /// Abstract class is used to set base for shapes, so they share the x and y coordinates.
    /// Abstract calss inherits from Interface class, and must pass or implement the method.
    /// </summary>
    public abstract class CommandAbstract : CommandInterface
    {
        protected Color colour;
        protected int x, y;
        protected int sideA, sideB;
        
        /// <summary>
        /// Default constructor used to set base values for colour, x and y
        /// </summary>
        public CommandAbstract()
        {
            colour = Color.Red;
            x = y = 100;
        }

        /// <summary>
        /// Method used to assign values for parameters
        /// </summary>
        /// <param name="colour">Colour string used to set colour for the pen</param>
        /// <param name="x">x coordiantes for pointer</param>
        /// <param name="y">y coordinates for pointer</param>
        public virtual void set(Color colour, params int[] list)
        {
            try
            {
                this.colour = colour;
                this.x = list[0];
                this.y = list[1];
                this.sideA = list[2];
                this.sideB = list[3];
            }
            catch (Exception e) { }


        }
        /// <summary>
        /// Must be implemented or passed
        /// </summary>
        /// <param name="g">used to override graphics</param>
        public abstract void draw(Graphics g);

    }
}
