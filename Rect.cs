﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgrammingLanguageEnvironment
{
    /// <summary>
    /// This class draws the rectange, once the CommandFactory creates the command this class will run.
    /// </summary>
    public class Rect : CommandAbstract
    {
        /// <summary>
        /// base constructor
        /// </summary>
        public Rect() : base()
        {
        }
        /// <summary>
        /// Draws the rectangle on the canvas
        /// </summary>
        /// <param name="p">Uses the pen</param>
        /// <example>g.DrawRectangle(p, x, y, sideA, sideB);</example>
        public override void draw(Graphics g)
        {
            Pen p = new Pen(Color.Black, 2);
            g.DrawRectangle(p, x, y, sideA, sideB);
        }

    }
}
