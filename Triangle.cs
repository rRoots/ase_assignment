﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgrammingLanguageEnvironment
{
    /// <summary>
    /// The triangle class is used to set draw a triangle on the canvas.
    /// the Circle class derrives from the Shape class, therefore it must contain the same methods as the Interface class
    /// </summary>
    public class Triangle : CommandAbstract

    {
        int radius;

        /// <summary>
        /// Base constructor
        /// </summary>
        public Triangle() : base()
        {

        }

        /// <summary>
        /// sets the x and y coordinates in the Abstract class and also sets the colour of the pen
        /// </summary>
        /// <param name="colour"></param>
        /// <param name="list"></param>
        /// 
        public override void set(Color colour, params int[] list)
        {
            //list[0] is x, list[1] is y, list[2] is radius
            base.set(colour, list[0], list[1]);
            this.radius = list[2];
        }

        /// <summary>
        /// Interface method, draws the shape on the canvas once it is called
        /// </summary>
        /// <param name="g">Overrides the graphics</param>
        /// <param name="pnt">sets the points of the triangle ith x and y</param>
        /// <example>pnt[0].X = x;</example>
        public override void draw(Graphics g)
        {

            Point[] pnt = new Point[3];

            pnt[0].X = x;
            pnt[0].Y = y;

            pnt[1].X = x - 25;
            pnt[1].Y = y + 30;

            pnt[2].X = x + 30;
            pnt[2].Y = y + 30;

            Pen p = new Pen(Color.Black, 2);

            g.DrawPolygon(p, pnt);

        }

    }
}
