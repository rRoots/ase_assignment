﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgrammingLanguageEnvironment
{
    /// <summary>
    /// CustomeExceotions class uses the exception class to handle the thrown exceptions throughout the program.
    /// </summary>
    [Serializable]
    class CustomExceptions : Exception
    {
        static string errorPass = " ";
        /// <summary>
        /// default constructor
        /// </summary>
        public CustomExceptions()
        {

        }
        /// <summary>
        /// Passess the errors to the interface that handeles the outputting
        /// </summary>
        public static string errorMsg   // used to set/get any error messages
        {
            get { return errorPass; }   // get method
            set { errorPass = value; }  // set method
        }
        /// <summary>
        /// Custom exception, it takes 3 parameters
        /// </summary>
        /// <param name="invalidString"> Uses the string thrown by the throw command</param>
        /// <param name="command"> uses the command passed</param>
        /// <param name="lineNo"> shows which line the error occured on</param>
        public CustomExceptions(string invalidString, string command, int lineNo)
        {

            if (invalidString.Equals("Invalid command.")) { errorPass = "Invalid command: " + command + " on line: " + lineNo; }
            if (invalidString.Equals("Incorrect parameter.")) { errorPass = "Incorrect parameter: " + command + " on line: " + lineNo; }
            if (invalidString.Equals("Incorrect comparison symbol.")) { errorPass = "Incorrect comparison symbol: " + command + " on line: " + lineNo; }
        }
        /// <summary>
        /// Custom exception, it takes 3 parameters
        /// </summary>
        /// <param name="invalidString"> Uses the string thrown by the throw command</param>
        /// <param name="command"> uses the command passed</param>
        /// <param name="lineNo"> shows which line the error occured on</param>
        public CustomExceptions(string invalidString, int command, int lineNo)
        {
            if (invalidString.Equals("Invalid command.")) { errorPass = "Invalid command: " + command + " on line: " + lineNo; }
            if (invalidString.Equals("Incorrect parameter.")) { errorPass = "Incorrect parameter: " + command + " on line: " + lineNo; }

        }
        /// <summary>
        /// Custom exception, it takes 4 parameters
        /// </summary>
        /// <param name="invalidString">Uses the string thrown by the throw command</param>
        /// <param name="command"> Integer one, usually side A</param>
        /// <param name="commandTwo">Integer two, usually side B</param>
        /// <param name="lineNo"> shows which line the error occured on</param>
        public CustomExceptions(string invalidString, int command, int commandTwo, int lineNo)
        {
            if (invalidString.Equals("Invalid command.")) { errorPass = "Invalid command: " + command + " on line: " + lineNo; }
            if (invalidString.Equals("Incorrect parameter.")) { errorPass = "Incorrect parameters: " + command + "," + commandTwo + " on line: " + lineNo; }

        }
        /// <summary>
        /// Custom exception, it takes 2 parameters
        /// </summary>
        /// <param name="invalidString"> Uses the string thrown by the throw command</param>
        /// <param name="lineNo"> shows which line the error occured on</param>
        public CustomExceptions(string invalidString, int lineNo)

        {
            if (invalidString.Equals("Missing parameter.")) { errorPass = "Missing parameter on line: " + lineNo; }
            if (invalidString.Equals("Invalid command.")) { errorPass = "Invalid command on line: " + lineNo; }
            if (invalidString.Equals("Incorrect parameter.")) { errorPass = "Incorrect parameters on line: " + lineNo; }

        }
        /// <summary>
        /// Custom exception, takes 2 parameters, it is used to report back invalid shapes
        /// </summary>
        /// <param name="invalidString">Uses the string thrown by the throw command</param>
        /// <param name="shape">the invalid shape</param>
        public CustomExceptions(string invalidString, string shape) 
        {
            errorPass = invalidString + shape;
        }


    }
}
