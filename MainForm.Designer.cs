﻿namespace ProgrammingLanguageEnvironment
{
    partial class mainProgram
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.outputBox = new System.Windows.Forms.PictureBox();
            this.commandLine = new System.Windows.Forms.TextBox();
            this.programBox = new System.Windows.Forms.RichTextBox();
            this.errorOutput = new System.Windows.Forms.TextBox();
            this.syntaxButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.outputBox)).BeginInit();
            this.SuspendLayout();
            // 
            // outputBox
            // 
            this.outputBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.outputBox.Location = new System.Drawing.Point(388, 43);
            this.outputBox.Name = "outputBox";
            this.outputBox.Size = new System.Drawing.Size(454, 503);
            this.outputBox.TabIndex = 0;
            this.outputBox.TabStop = false;
            this.outputBox.Paint += new System.Windows.Forms.PaintEventHandler(this.outputBox_Paint);
            // 
            // commandLine
            // 
            this.commandLine.Location = new System.Drawing.Point(388, 591);
            this.commandLine.Name = "commandLine";
            this.commandLine.Size = new System.Drawing.Size(454, 20);
            this.commandLine.TabIndex = 1;
            this.commandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.commandLine_KeyDown);
            // 
            // programBox
            // 
            this.programBox.Location = new System.Drawing.Point(34, 43);
            this.programBox.Name = "programBox";
            this.programBox.Size = new System.Drawing.Size(314, 307);
            this.programBox.TabIndex = 2;
            this.programBox.Text = "";
            // 
            // errorOutput
            // 
            this.errorOutput.Location = new System.Drawing.Point(34, 384);
            this.errorOutput.Multiline = true;
            this.errorOutput.Name = "errorOutput";
            this.errorOutput.Size = new System.Drawing.Size(314, 284);
            this.errorOutput.TabIndex = 3;
            // 
            // syntaxButton
            // 
            this.syntaxButton.Location = new System.Drawing.Point(544, 624);
            this.syntaxButton.Name = "syntaxButton";
            this.syntaxButton.Size = new System.Drawing.Size(139, 29);
            this.syntaxButton.TabIndex = 4;
            this.syntaxButton.Text = "Check Syntax";
            this.syntaxButton.UseVisualStyleBackColor = true;
            this.syntaxButton.Click += new System.EventHandler(this.syntaxButton_Click);
            // 
            // mainProgram
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 680);
            this.Controls.Add(this.syntaxButton);
            this.Controls.Add(this.errorOutput);
            this.Controls.Add(this.programBox);
            this.Controls.Add(this.commandLine);
            this.Controls.Add(this.outputBox);
            this.Name = "mainProgram";
            this.Text = "Drawing with colours";
            ((System.ComponentModel.ISupportInitialize)(this.outputBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox outputBox;
        private System.Windows.Forms.TextBox commandLine;
        private System.Windows.Forms.RichTextBox programBox;
        private System.Windows.Forms.TextBox errorOutput;
        private System.Windows.Forms.Button syntaxButton;
    }
}

