﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;

namespace ProgrammingLanguageEnvironment
{
    //Canvas class displays visuals based on the programming language that is inputted into the command line
    /// <summary>
    /// Class is used to interact with the Drawing class and other members of the CommandAbstact class.
    /// </summary>
    public class Canvas : CommandAbstract
    { 
        Graphics gp; //graphics context
        Pen pn; //pen
        Brush brs; //brush
        CommandAbstract s;
        ArrayList shapes = new ArrayList();
        CommandFactory factory = new CommandFactory();
        Color newColour = Color.Black;//creates default pen
        private int xPos = 0; //x position of the cursor
        private int yPos = 0; //y position of the cursor
        public string penColour = "black"; //defult pen colour is black
        public string filler = "off"; //filler is off by defualt

        // Constructor uses Graphics class
        /// <summary>
        /// Constructor, used to pass the graphics.
        /// </summary>
        /// <param name="gp">Passing the graphics</param>
        /// <param name="xPos">x position of the pointer</param>#
        /// <param name="pn">creates a pen</param>
        /// <param name="brs"> creates a brush</param>
        public Canvas(Graphics gp)
        {
            this.gp = gp;
            xPos = xPos + 0;
            pn = new Pen(Color.Black, 1);
            brs = new System.Drawing.SolidBrush(System.Drawing.Color.Black);

        }
        /// <summary>
        /// draw method used to override the default draw method. And to draw the shapes on the canvas.
        /// </summary>
        /// <param name="gp">Used to pass the graphics</param>
        /// <param name="pn">creates a pen</param>
        /// <param name="b"> creates a brush</param>
        public override void draw(Graphics gp)
        {

            Pen p = new Pen(Color.Black, 2);
            SolidBrush b = new SolidBrush(colour);
            gp.FillEllipse(b, x, y, xPos * 2, xPos * 2);
            gp.DrawEllipse(p, x, y, xPos * 2, xPos * 2);

        }
        /// <summary>
        /// DrawRectangle method is responsible for drawing the rectangle on the canvas. It takes two variables, sideA and sideB.
        /// </summary>
        /// <param name="sideA">side A of the rectangle</param>
        /// <param name="sideB">side B of the rectangle</param>
        public void DrawRectangle(int sideA, int sideB)
        {
            if (filler.Equals("on"))
            {
            }
            else
            {
                gp.DrawRectangle(pn, xPos, yPos, sideA, sideB);
            }
        }
        /// <summary>
        /// The set method is used to set the 
        /// </summary>
        /// <param name="colour">Used to pass the colour</param>
        /// <param name="list">Used to pass a list of integers/variables</param>
        public override void set(Color colour, params int[] list) //param keyword give a wider parameter list
        {
            this.colour = colour;
            this.x = list[0];
            this.y = list[1];
        }
        /// <summary>
        /// Default constructor
        /// </summary>
        public Canvas()
        {
        }

        /// <summary>
        /// the xPost method is used to mainly help sending the x coordinates of the pointer between classes.
        /// </summary>
        /// <param name="xPos">x position of the pointer</param>

        public int xPosmethod    //used to get/set the xPos
        {
            get => xPos;
            set => xPos = value;
        }
        /// <summary>
        ///  the yPost method is used to mainly help sending the y coordinates of the pointer between classes.
        /// </summary>
        /// <param name="yPos">y position of the pointer</param>
        public int yPosmethod    //used to get/set the yPos
        {
            get => yPos;
            set => yPos = value;
        }

        /// <summary>
        /// Method to draw from point a to b
        /// </summary>
        /// <param name="toX">Draws from point x</param>
        /// <param name="toY">Draws to point y</param>
        public void DrawTo(int toX, int toY)
        {
            gp.DrawLine(pn, xPos, yPos, toX, toY);
            xPos = toX;
            yPos = toY;
        }

        //method to change pen colour if colour is incorrect it changes back to black
        /// <summary>
        /// Method used to pass a colour to it then the colour will set the colour for the pen
        /// </summary>
        /// <param name="colour"> String parameter for the pen colour</param
        /// <param name="pn"> Creates new pen.</param>
        /// <example>pn = new Pen(Color.Blue, 1);</example>
        public void PenColour(string colour)
        {
            if (colour.Equals("red"))
            {
                pn = new Pen(Color.Red, 1);
                brs = new System.Drawing.SolidBrush(System.Drawing.Color.Red);
            }
            else if (colour.Equals("blue"))
            {
                pn = new Pen(Color.Blue, 1);
                brs = new System.Drawing.SolidBrush(System.Drawing.Color.Blue);
            }
            else if (colour.Equals("purple"))
            {
                pn = new Pen(Color.Purple, 1);
                brs = new System.Drawing.SolidBrush(System.Drawing.Color.Purple);
            }
            else if (colour.Equals("green"))
            {
                pn = new Pen(Color.Green, 1);
                brs = new System.Drawing.SolidBrush(System.Drawing.Color.Green);
            }
            else
            {
                pn = new Pen(Color.Black, 1);
                brs = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
            }
        }

        /// <summary>
        /// method to draw circle, takes one variable, the radius
        /// </summary>
        /// <param name="radius">Parameter for the circle radius</param>
        /// <remarks>Returns command, sets positon for shape, adds the shape to the arraylist and draws the shape.</remarks>
        /// <example>s = factory.CreateCommand("circle");</example>
        public void DrawCircle(int radius)
        {
            //this method creates a command that is handled in the CommandFactory class.
            s = factory.CreateCommand("circle");
            s.set(newColour, xPos, yPos, radius, 10);
            shapes.Add(s);
            s.draw(gp);
        }
        /// <summary>
        /// method to draw triangle, takes 2 variables
        /// </summary>
        /// <param name="sideA">Side a of the triangle </param>
        /// <param name="sideB">Side b of the triangle</param>
        /// /// <remarks>Returns command, sets positon for shape, adds the shape to the arraylist and draws the shape.</remarks>
        /// <example>s = factory.CreateCommand("triangle");</example>
        public void DrawTriangle(int sideA, int sideB)
        {
            s = factory.CreateCommand("triangle");
            s.set(newColour, xPos, yPos, sideA, sideB);
            shapes.Add(s);
            s.draw(gp);
        }
        /// <summary>
        /// method to draw rectangle, takes 2 variables, this method similar to other rectangle method except this one uses the command factory.
        /// </summary>
        /// <param name="sideA"></param>
        /// <param name="sideB"></param>
        /// <remarks>Returns command, sets positon for shape, adds the shape to the arraylist and draws the shape.</remarks>
        /// <example> s = factory.CreateCommand("rect");</example>
        public void DrawRect(int sideA, int sideB)
        {
            s = factory.CreateCommand("rect");
            s.set(newColour, xPos, yPos, sideA, sideB);
            shapes.Add(s);
            s.draw(gp);
        }
        /// <summary>
        /// method to clear the graphical panel
        /// </summary>
        /// <remarks>Clears the canvas by painting it white.</remarks>
        public void Clear()
        {
            gp.Clear(Color.White);
        }
        /// <summary>
        /// method to reset the position of the cursor to default
        /// </summary>
        /// <remarks>Resets x and y positions of the pointer.</remarks>
        public void Reset()
        {
            xPos = 0;
            yPos = 0;
        }
    }
}
