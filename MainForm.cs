﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgrammingLanguageEnvironment
{
    /// <summary>
    /// mainProgram class is used to interact with the form and handle processing of the lines, which then passed to canvas for drawing.
    /// </summary>
    /// <completionlist cref=""/>
    public partial class mainProgram : Form
    {
        /// <summary>
        /// Refferences the Command Factory
        /// </summary>
        CommandFactory sF = new CommandFactory();
        Bitmap OutputBitmap = new Bitmap(800, 450); //bitmap default size
        Canvas myCanvas; //references the canvas class
        int lineCounter; // counts the lines
        int varCounter = 0; //counts variables
        String[] varString = new string[50]; // used to store variables
        int[] varInt = new int[50]; // used to convert variables
        int index = -1; // sets the index to -1
        bool varFlag = false; // set to true if program detects variables
        bool varID = false; // used to handle variables
        int loopInt = 0;// the number if times the loop needs to work
        bool loop = false; // detects loop
        int forInt = 0; // number reserved for loops
        bool commandFlag = false; //wheter the variable is a command
        string loopCommandInt; //used to store commands that will be converted to integer
        string loopString; //string that will be looped
        bool loopCommand = false; // detects if loop has been started
        string loopLine; // line to loop
        bool loopBool = false; //detects if loop line has been assigned
        bool circleLoop = false; //wheter circle has been looped
        bool ifFlag = true; // program detects if command
        bool syntaxFlag = false; //used to set program to do dry run
        bool exception = false; //detect wheter exception has been generated
        bool wordChecker = false; //if set to false, program will report invalid word
        bool methodChecker = false;//checks for method
        string methodSideAString = " ", methodSideBString, methodType, methodName = " "; //stings created to store the method strings
        int methodSideA, methodSideB; // integers to store ints from the method
        int methodCounter = 0;// counter for method
        String[] methodLines = new string[20]; // lines that are included in the method
        String[] methodSplitArray = new string[20]; // split array for method
        bool endMethod = false; // flag to end method
        string methodStringCommandList = "empty"; //used in logic to avoid excecution
        bool specialSplit = false; // if lines have been split
        string methodStringCommand; // stores method command

        /// <summary>
        /// initializes the form and allows the current bitmap to draw on by calling myCanvas
        /// </summary>
        public mainProgram()
        {
            InitializeComponent();
            myCanvas = new Canvas(Graphics.FromImage(OutputBitmap));
        }
        /// <summary>
        /// Saves the image
        /// </summary>
        public void SaveImage()
        {
            // Create a SaveFileDialog to request a path and file name to save to.
            SaveFileDialog saveRichTextBox = new SaveFileDialog();

            // Initialize the SaveFileDialog to specify the RTF extention for the file.
            saveRichTextBox.DefaultExt = "*.rtf";
            saveRichTextBox.Filter = "RTF Files|*.rtf";

            // Determine whether the user selected a file name.
            if (saveRichTextBox.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
               saveRichTextBox.FileName.Length > 0)
            {
                // Save the contents of the RichTextBox into the file.
                programBox.SaveFile(saveRichTextBox.FileName);
                // File.WriteAllText(@"asd.rtf", programBox.Text);
            }
        }
        /// <summary>
        /// Loads the iamge
        /// </summary>
        public void LoadImage()
        {
            // Create an OpenFileDialog to request a file to open.
            OpenFileDialog openRichTextBox = new OpenFileDialog();

            // Initialize the OpenFileDialog to look for RTF files.
            openRichTextBox.DefaultExt = "*.rtf";
            openRichTextBox.Filter = "RTF Files|*.rtf";

            // Determine whether the user selected a file from the OpenFileDialog.
            if (openRichTextBox.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
               openRichTextBox.FileName.Length > 0)
            {
                // Load the contents of the file into the RichTextBox.
                programBox.LoadFile(openRichTextBox.FileName);
            }
        }

        /// <summary>
        /// Event handler
        /// </summary>
        /// <param name="sender"> Sends it to the handler</param>
        /// <param name="e">Key events detected</param>
        private void commandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)//enter pressed down for command line
            {
                errorOutput.Clear();//clears textbox
                String[] stringArray = new string[5];
                char[] delimiterChars = { ' ', ',', '.', ':' };//delimiter characters
                String Command = commandLine.Text.ToLower();
                stringArray = Command.Split(delimiterChars);//splits command

                if (stringArray[0].Equals("moveto")) //moveto command 
                {
                    int xPos, yPos;
                    if (Int32.TryParse(stringArray[1], out xPos) != true || Int32.TryParse(stringArray[2], out yPos) != true)
                    {
                        errorOutput.AppendText("Enter appropriate parameter."); //returns with incorrect paramaters
                    }
                    else
                    {
                        myCanvas.xPosmethod = xPos;//moves cursor
                        myCanvas.yPosmethod = yPos;//moves cursor
                    }

                }
                else if (stringArray[0].Equals("circle") == true)
                {
                    int radius;
                    if (Int32.TryParse(stringArray[1], out radius) != true)
                    {
                        errorOutput.AppendText("Enter appropriate parameter."); //returns with incorrect paramaters
                    }
                    else
                    {
                        myCanvas.DrawCircle(radius); //calles DrawCircle method from Canvas class and passes 1 parameter
                    }

                }
                else if (stringArray[0].Equals("pen") == true)
                {
                    myCanvas.PenColour(stringArray[1]); //sets Pen for Canvas Class
                }
                else if (stringArray[0].Equals("fill") == true)
                {
                    myCanvas.filler = stringArray[1]; // calles filler method from Canvas class
                }
                else if (stringArray[0].Equals("triangle") == true)
                {
                    int sideA, sideB;
                    if (Int32.TryParse(stringArray[1], out sideA) != true || Int32.TryParse(stringArray[2], out sideB) != true)
                    {
                        errorOutput.AppendText("Enter appropriate parameter."); //returns with incorrect paramaters
                    }
                    else
                    {
                        myCanvas.DrawTriangle(sideA, sideB); //calles drawTriangle method from Canvas class and passes 2 parameters
                    }


                }
                else if (stringArray[0].Equals("rect") == true)
                {
                    int sideA, sideB;
                    if (Int32.TryParse(stringArray[1], out sideA) != true || Int32.TryParse(stringArray[2], out sideB) != true)
                    {
                        errorOutput.AppendText("Enter appropriate parameter."); //returns with incorrect paramaters
                    }
                    else
                    {
                        myCanvas.DrawRect(sideA, sideB);
                    }

                }
                else if (Command.Equals("clear") == true)
                {
                    myCanvas.Clear(); //calles clear method from Canvas class
                }
                else if (Command.Equals("reset") == true)
                {
                    myCanvas.Reset();//calles reset method from Canvas class
                }
                else if (stringArray[0].Equals("drawto") == true)
                {
                    int xPos, yPos;
                    if (Int32.TryParse(stringArray[1], out xPos) != true || Int32.TryParse(stringArray[2], out yPos) != true)
                    {
                        errorOutput.AppendText("Enter appropriate parameter."); //returns with incorrect paramaters
                    }
                    else
                    {
                        myCanvas.DrawTo(xPos, yPos);
                    }

                }
                else if (Command.Equals("run") == true)
                {
                    lineCounter = 0;
                    syntaxFlag = false;
                    String[] commands = new String[30];
                    String[] split = new String[30];
                    for (int i = 0; i < programBox.Lines.Length; i++)//passes the contect of programBox (multy line textbox) to the parser method
                    {
                        commands[i] = programBox.Lines[i];
                        LineParser(commands[i]); //iterates through parser class 1 by 1
                    }
                }
                else if (Command.Equals("save") == true)
                {
                    SaveImage();//calles save method
                }
                else if (Command.Equals("load") == true)
                {
                    LoadImage();//calles load method
                }
                else
                {
                    errorOutput.AppendText("Enter appropriate command.");//returns with incorrect command
                }
                commandLine.Text = ""; //clear the command line


                Refresh(); //refreshes screen

            } // end of Enter input
        }
        /// <summary>
        /// Syntax button used to check lines
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event listener</param>
        private void syntaxButton_Click(object sender, EventArgs e)
        {
            lineCounter = 0;
            syntaxFlag = true;
            String[] commands = new String[30];
            String[] split = new String[30];
            for (int i = 0; i < programBox.Lines.Length; i++)//passes the contect of programBox (multy line textbox) to the parser method
            {
                commands[i] = programBox.Lines[i];
                LineParser(commands[i]); //iterates through parser class 1 by 1
            }
        }
        /// <summary>
        /// Passes a line to perform a test
        /// </summary>
        /// <param name="line">Passes line to LineParser for the test</param>
        public void TestSender(string line) { LineParser(line); } //sends the line from UnitTesting to parser method
        /// <summary>
        /// Sends the positon of the x and y coordinates
        /// </summary>
        public void TestPositonSender() { PassClass.xPasser = myCanvas.xPosmethod; PassClass.yPasser = myCanvas.yPosmethod; } // sends the x,y positions for UnitTesting class
        /// <summary>
        ///parses thorough a line that is sent in
        /// </summary>
        /// <param name="line"> line that is passed in</param>
        public void LineParser(String line) 
        {
            try
            {

                if (exception == false) // only runs if exception has been hanled.
                {
                    wordChecker = false; //cheks for words, used to report back incorrect commands
                    varID = false; // detects variables
                    lineCounter++; // line counter increased
                    line = line.ToLower().Trim(); // trims line
                    String command = " ";
                    string[] splitArray = new string[12];
                    String[] parameters = new string[12];
                    int[] intParameter = new int[5];
                    splitArray = line.Split(' '); // splits line
                    // if line beginds with end if
                    if (line.Equals("endif")) { wordChecker = true; ifFlag = true; } 
                    // if line beginds with endmethod, flags will be changed
                    if (line.Equals("endmethod")) { wordChecker = true; methodChecker = false; endMethod = true; }
                    // if splitArray[0] is equal to method the following block will be executed 
                    if (splitArray[0].Equals("method"))
                    {
                        wordChecker = true;
                        methodChecker = true;
                        splitArray = line.Split(' ', ','); // splits array again, but with different options
                        if (splitArray[0].Equals("method") && !splitArray[1].Equals(null))
                        {
                            methodName = splitArray[1];
                            if (splitArray[3].Equals("int"))
                            {
                                methodSideAString = splitArray[4]; //sets one of the inputted variables to a integer that is stored while the program runs
                            }
                            if (splitArray[5].Equals("int"))
                            {
                                methodSideBString = splitArray[6]; //sets the other inputted variables to a integer that is stored while the program runs
                            }

                            if (splitArray[8].Equals("string")) // sets the method command 
                            {
                                methodType = splitArray[9];
                            }
                        }
                        else //any other option will throw an exceptions
                        {
                            exception = true;
                            throw new CustomExceptions("Incorrect parameter.", splitArray[1], lineCounter); // handled in CustomException class
                        }
                    }

                    if (methodName.Equals(splitArray[0]) || methodSideAString.Equals(splitArray[0]))
                    {

                        wordChecker = true; // to avoid flagging up for incorrect word

                        // if the line is incrementing an already existing variable
                        if (splitArray[0].Equals(methodSideAString) && !methodStringCommandList.Equals("empty")) 
                        {
                            int temp;
                            Int32.TryParse(splitArray[3], out temp);
                            methodSideA = +temp;
                            //creates a string that will be passed to the program again to draw next command
                            methodStringCommandList = methodStringCommandList + " " + methodType + " " + methodSideA + " "; 
                            LineParser(methodStringCommandList);
                        }
                        // if method is not incrementing, but drawing
                        else if (!methodLines[1].Equals(null) && endMethod == true)
                        {
                            string temp;
                            methodSplitArray = methodLines[1].Split(' ');
                            methodType = splitArray[6];


                            if (methodSplitArray[1].Equals(methodSideAString)) { Int32.TryParse(splitArray[2], out methodSideA); }
                            //uses previulsy assigned variables to create another line that will be executed by the program
                            methodStringCommandList = methodType + " " + methodSideA;
                            int tempInt = methodSideA; //storing value of sideA variable
                            temp = methodLines[methodCounter - 3];// previously run command
                            splitArray = methodLines[2].Split(' '); // splits command
                            Int32.TryParse(splitArray[4], out methodSideA); //converts from string to int
                            tempInt = tempInt + methodSideA; //ands converted int to total
                            methodStringCommand = methodType + " " + tempInt;
                            specialSplit = true;
                            //line passed to the Parser
                            LineParser(methodStringCommandList);
                        }
                    }
                    //runs if the method checker flag is set to false
                    if (methodChecker == false)
                    {
                        if (splitArray.Length == 2) //splits the contect of the line 
                        {
                            wordChecker = true;
                            command = splitArray[0];
                            parameters = splitArray[1].Split(',');

                            // used to parse through variables
                            for (int i = 0; i < parameters.Length; i++)
                            {

                                if (Int32.TryParse(parameters[i], out intParameter[i]) != true)
                                {
                                }
                                varID = true;
                                if (varInt[varCounter] == (intParameter[i]))
                                {
                                    varFlag = true; //sets varFlag to true
                                }
                                if (loop == true && loopCommand == false) //logic used for loops
                                {
                                    loopString = command;
                                    loopCommand = true;
                                }
                            }
                            if (command.Equals("end")) { wordChecker = true; } //turns the flag off so the for loop stops
                        }
                        if (splitArray.Length >= 3)
                        {
                            command = splitArray[0];

                            if (splitArray[1].Equals("=")) // used to assign values to variables 
                            {
                                wordChecker = true;
                                if (commandFlag == false)
                                {
                                    loopCommandInt = command; //integer in string format, for it to be passed to the parser
                                }
                                commandFlag = true; // sets commandFlg to true so previous loop if is not repeated and overwritten

                                for (int i = 0; index < varCounter; index++)
                                {
                                    i++;
                                    varString[varCounter] = command;
                                    if (Int32.TryParse(splitArray[2], out varInt[varCounter]) != true)
                                    {
                                        if (varInt[varCounter] != 0)
                                        {
                                            exception = true; // exception thrown if the conversion fails
                                            throw new CustomExceptions("Incorrect parameter.", varInt[varCounter], lineCounter);
                                        }
                                    }
                                }
                                if (splitArray.Length == 5 && !line.Equals(methodLines[0])) // if the length of the line is 5 
                                {
                                    if (splitArray[3].Equals("+"))
                                    {
                                        int add = 0;
                                        // if statements used to handle the inctementing of the variables
                                        if (loop == true && loopInt > 0)
                                        {
                                            if (varString[varCounter].Equals(command))
                                            {
                                                Int32.TryParse(splitArray[4], out add);//converts
                                                forInt = forInt + add;
                                            }
                                            if (loopBool == false && loop == true)
                                            {
                                                loopLine = line; //sets the loopLine to the current line
                                                loopBool = true; // turns flag on to avoid overriding
                                            }
                                        }
                                        else
                                        {
                                            if (varString[varCounter].Equals(command)) // if the command matches the variable
                                            {
                                                Int32.TryParse(splitArray[4], out add);
                                                varInt[varCounter] = varInt[varCounter - 1] + add; //increments the loop by the required number

                                            }
                                        }
                                    }
                                    if (circleLoop == false && loopInt > 0)
                                    {
                                        circleLoop = true;
                                        LineParser(loopString + " " + loopCommandInt);
                                    }
                                }
                                varCounter++;
                            }
                            if (command.Equals("loop")) //handles logic for comparison in the loop
                            {
                                wordChecker = true;
                                loop = true;
                                Int32.TryParse(splitArray[2], out loopInt);
                                loopInt = loopInt - 1;
                            }
                            if (command.Equals("if"))
                            {
                                wordChecker = true;
                                ifFlag = false;
                                if (splitArray[2].Equals("<"))
                                {
                                    int a, b;
                                    a = varInt[varCounter - 1];
                                    Int32.TryParse(splitArray[3], out b);

                                    if (a < b) { ifFlag = true; } //turns on if flag so the commands are excecuted afterwards
                                }
                                else if (splitArray[2].Equals(">"))
                                {
                                    int a, b;
                                    a = varInt[varCounter - 1];
                                    Int32.TryParse(splitArray[3], out b);

                                    if (a > b) { ifFlag = true; }//turns on if flag so the commands are excecuted afterwards
                                }
                                else if (splitArray[2].Equals("<="))
                                {
                                    int a, b;
                                    a = varInt[varCounter - 1];
                                    Int32.TryParse(splitArray[3], out b);

                                    if (a <= b) { ifFlag = true; }//turns on if flag so the commands are excecuted afterwards
                                }
                                else if (splitArray[2].Equals(">="))
                                {
                                    int a, b;
                                    a = varInt[varCounter - 1];
                                    Int32.TryParse(splitArray[3], out b); //converting from string to integer

                                    if (a >= b) { ifFlag = true; }//turns on if flag so the commands are excecuted afterwards
                                }
                                else
                                {
                                    ifFlag = true; //anything else is reported as incorrect
                                    throw new CustomExceptions("Incorrect comparison symbol.", splitArray[2], lineCounter);
                                }

                            }

                        }
                    }
                    // this is entered if the program dectes method
                    else
                    { 
                        wordChecker = true;
                        methodLines[methodCounter] = line; // stores the line in the method storage.
                        methodCounter++;
                    }
                    if (varID == true && ifFlag == true)//only works if these flags are turned on
                    {
                        if (command.Equals("drawto") == true)
                        {
                            wordChecker = true;
                            if (intParameter[2] == 0)
                            {
                                if (syntaxFlag == false)// if syntax flag is turned on it means that it will not draw
                                {
                                    myCanvas.DrawTo(intParameter[0], intParameter[1]); // passes the parameters to the canvas ckass
                                }
                            }
                        }
                        else if (command.Equals("circle") == true)
                        {
                            wordChecker = true;
                            if (intParameter[1] == 0)
                            {
                                if (syntaxFlag == false)
                                {
                                    //used to handle the loop
                                    if (circleLoop == true && varFlag == true && loopInt > 0 && syntaxFlag == false)
                                    {
                                        myCanvas.DrawCircle(forInt); loopInt--; Refresh(); LineParser(loopLine); Refresh(); LineParser(loopString + " " + loopCommandInt); Refresh();

                                    }
                                    //draws a circle with any given variable
                                    else if (varFlag == true && circleLoop == false && syntaxFlag == false) { myCanvas.DrawCircle(varInt[varCounter - 1]); forInt = +varInt[varCounter - 1]; Refresh(); }
                                    //handles drawing a circle with a integer
                                    else if (varFlag == false && circleLoop == false && syntaxFlag == false) { myCanvas.DrawCircle(intParameter[0]); }//
                                }
                            }
                            else
                            {
                                // anything else will result in a thrown exceptions
                                exception = true;
                                throw new CustomExceptions("Incorrect parameter.", lineCounter);
                            }
                            if (intParameter[1] == 0)
                            {
                                if (syntaxFlag == false)
                                {// this is used when all flags are turned down.
                                    myCanvas.DrawCircle(intParameter[0]);
                                }

                            }
                            //anything else throws an exceptions
                            else { exception = true; throw new CustomExceptions("Incorrect parameter.", intParameter[0], lineCounter); }
                        }
                        else if (command.Equals("tri") == true)
                        {
                            wordChecker = true;
                            if (intParameter[2] == 0)
                            {
                                if (syntaxFlag == false)
                                {
                                    //triangle command will send the parameters to the canvas class which then handles it
                                    myCanvas.DrawTriangle(intParameter[0], intParameter[1]);
                                }

                            }
                            //else exception is thrown
                            else { exception = true; throw new CustomExceptions("Incorrect parameter.", intParameter[0], lineCounter); }
                        }
                        else if (command.Equals("moveto") == true) //move to command
                        {
                            wordChecker = true;
                            if (intParameter[2] == 0)
                            {
                                if (syntaxFlag == false)
                                {
                                    //sets x and y coordinates
                                    myCanvas.xPosmethod = intParameter[0];
                                    myCanvas.yPosmethod = intParameter[1];
                                }

                            }
                            else { exception = true; throw new CustomExceptions("Incorrect parameter.", intParameter[0], intParameter[1], lineCounter); }
                        }
                        //handles the drawing of the rectangle shape, it passes the parameters to the canvas class
                        else if (command.Equals("rect") == true)
                        {
                            wordChecker = true;
                            if (intParameter[0] == 0 || intParameter[1] == 0)
                            {
                                exception = true;
                                // exceotion if any of the parameters are missing
                                throw new CustomExceptions("Missing parameter.", lineCounter);
                            }
                            if (intParameter[2] == 0)
                            {
                                if (syntaxFlag == false)
                                {
                                    myCanvas.DrawRectangle(intParameter[0], intParameter[1]);
                                }
                            }
                            else
                            {
                                exception = true;//exception if the parameter is incorrect
                                throw new CustomExceptions("Incorrect parameter.", intParameter[0], intParameter[1], lineCounter);
                            }
                        }
                        //clears the canvas
                        else if (command.Equals("clear") == true)
                        {
                            wordChecker = true;
                            if (syntaxFlag == false)
                            {
                                myCanvas.Clear();
                            }
                        }
                        //resets the pointer
                        else if (command.Equals("reset") == true)
                        {
                            wordChecker = true;
                            if (syntaxFlag == false)
                            {
                                myCanvas.Reset();
                            }
                        }
                        else
                        {
                            //if word checker is set to false somewhere then invalid command is returned
                            if (wordChecker != true)
                            {
                                throw new CustomExceptions("Invalid command.", command, lineCounter);
                            }
                        }


                    }
                    Refresh();//refreshes canvas
                    if (specialSplit == true)
                    {
                        specialSplit = false;
                        //used to pass any remaining method commands to the parser method.
                        LineParser(methodStringCommand);
                    }
                    if (wordChecker == false)
                    {
                        //if word checker is set to false somewhere then invalid command is returned
                        throw new CustomExceptions("Invalid command.", line, lineCounter);
                    }
                }
                else
                {
                    //updates error window if exception is true
                    updateError();
                }
            }
            catch (Exception e) { updateError(); }//catches exceptions and updates the error output
        }


        public void updateError()
        {
            //updates the text box with the error messages from the CustomException class
            errorOutput.AppendText(CustomExceptions.errorMsg + "\r\n");
            exception = false;
        }

        //paints on the canvas
        private void outputBox_Paint(object sender, PaintEventArgs e)
        {
            Graphics gp = e.Graphics; //get the displayed context of the form
            gp.DrawImageUnscaled(OutputBitmap, 0, 0);
        }


    }

}
