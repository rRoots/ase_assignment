﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgrammingLanguageEnvironment
{
    /// <summary>
    /// interface class which is used to allow adding more shapes to the program with ease
    /// </summary>
    interface CommandInterface
    {/// <summary>
    ///all derriving classes are forced to impelemt or pass this method, making it easiser to implement shapes
    /// </summary>
    /// <param name="g"></param>
        void draw(Graphics g);
    }
}

