﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgrammingLanguageEnvironment
{
    /// <summary>
    /// Command Factory, a shape is passed in and the factory will call a class or return the incorrect shape.
    /// </summary>
    public class CommandFactory
    {
        public static string returnShape;
        /// <summary>
        /// CommandAbstact references the abstract class where the commands are passed from
        /// </summary>
        /// <param name="CommandString"> Shape that is passed in.</param>
        /// <returns>returns a shape (calls one of the shape classes).</returns>
        public CommandAbstract CreateCommand(String CommandString)
        {
            CommandString = CommandString.ToLower().Trim();
            if (CommandString.Equals("rect")) { returnShape = "Rectangle returned."; return new Rect(); } //returns the Rect class
            else if (CommandString.Equals("triangle")) { returnShape = "Triangle returned."; return new Triangle(); }// returns the Triangle class
            else if (CommandString.Equals("circle")) { returnShape = "Circle returned.";  return new Circle(); } //returns the Circle class
            else
            {
                returnShape = "Invalid shape.";
                System.ArgumentException argEx = new ArgumentException("Invalid command: +" + CommandString);
                throw argEx;//throws invalid shape, which is handled in CustomExceptions class.
            }


        }

    }
}