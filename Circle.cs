﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgrammingLanguageEnvironment
{
    /// <summary>
    /// Circle class to draw circles on the canvas
    /// the Circle class derrives from the Shape class, therefore it must contain the same methods as the Interface class
    /// </summary>
    public class Circle : CommandAbstract
    {
        int radius; // radius, local variable
        public static int radiusTest = 0; // used in testing
        public static string invalidRadius = ""; //used in testing

        /// <summary>
        /// base constructor
        /// </summary>
        public Circle() : base()
        {

        }

        /// <summary>
        /// set method to set the base variables such as x and y in the abstact class.
        /// it is also used to set the colour of the shape
        /// </summary>
        /// <param name="colour"> variable to set colour</param>
        /// <param name="list"> list of integers</param>
        public override void set(Color colour, params int[] list)
        {
            //list[0] is x, list[1] is y, list[2] is radius
            base.set(colour, list[0], list[1]);
            this.radius = list[2];
            radiusTest = list[2];

            if (radius > 1000 || radius < 1000 ) { invalidRadius = "Invalid radius."; } //used for testing
        }

        /// <summary>
        /// Interface method, draws the shape on the canvas once it is called
        /// </summary>
        /// <param name="g"></param>
        public override void draw(Graphics g)
        {

            Pen p = new Pen(Color.Black, 2);
            g.DrawEllipse(p, x - radius, y - radius, radius + radius, radius + radius);

        }
    }
}